This is my StackOverFlow App.

Github is blocked on my office laptop so I made this project on Gitlab. Please visit https://gitlab.com/spriha27/my-stackoverflow to view commit history etc.

Routes:

1. /users/register: used to register users
2. /users/login: used to login users
3. /questions/create: used to create a question
4. /questions/answer: used to add an answer to the question
5. /questions/: displays all questions
6. /questions/displayQ: displays a single question
